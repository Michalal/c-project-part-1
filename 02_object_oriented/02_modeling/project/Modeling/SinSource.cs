using System;

namespace Modeling
{
    public class SinSource : ISource
    {
        private double _frequency;
        private double _phase;
        private double _amplitude;

        public SinSource(double frequency, double phase, double amplitude)
        {
            _frequency = frequency;
            _phase = phase;
            _amplitude = amplitude;
        }
        public double Frequency
        {
            get => _frequency;
        }
        public double Phase
        {
            get => _phase;
        }
        public double Amplitude
        {
            get => _amplitude;
        }
        public double Sample(double time)
        {
            var res = _amplitude * Math.Sin(time*2*(Math.PI ) *_frequency+_phase);
            return Math.Round(res, 2);
        }
    }
}