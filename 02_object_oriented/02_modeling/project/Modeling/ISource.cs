namespace Modeling
{
    public interface ISource
    {
        double Sample(double time);
    }
}