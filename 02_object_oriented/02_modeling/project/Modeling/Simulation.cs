using System;

namespace Modeling
{
    public class Simulation
    {
        private ISource _source;
        private ISeismogram _seismogram;

        public Simulation(ISource source, ISeismogram seismogram)
        {
            _source = source;
            _seismogram = seismogram;
        }

        public void Execute(double d, double d1, double d2)
        {
            if (d < d2)
            {
                while (d <= d2)
                {
                    var value = _source.Sample(d);
                    _seismogram.Store(d, value);
                    d += d1;
                }
            }
            else
            {
                while (d >= d2)
                {
                    var value = _source.Sample(d);
                    _seismogram.Store(d, value);
                    d += d1;
                }
            }
        }
    }
}