using System;
using System.IO;

namespace Modeling
{
    public class FileSeismogram : ISeismogram
    {
        private TextWriter _textWriter;

        public FileSeismogram(TextWriter file)
        {
            _textWriter = file;
        }

        public void Close()
        {
            _textWriter.Close();
        }
        
        public void Store(double time, double value)
        {
            _textWriter.WriteLine(time+" "+value);
        }
    }
}