using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Modeling
{
    public class FileSource : ISource
    {
        private readonly List<double> _time;
        private readonly List<double> _value;
       
        public FileSource(TextReader? textReader)
        {
            _time = new List<double>();
            _value = new List<double>();
            if (textReader == null)
            {
                throw new ArgumentNullException(nameof(textReader));
            }
 
            while (true)
            {
                string? _linia = textReader.ReadLine();
                if(_linia == null) 
                    break;
                string[] S = _linia.Split(' ');
                if (S.Length != 2)
                {
                    throw new InvalidFileException();
                }
 
                NumberFormatInfo provider = new()
                {
                    NumberDecimalSeparator = ".",
                    NumberGroupSeparator = ","
                };
                (_time).Add(Convert.ToDouble(S[0], provider));
                (_value).Add(Convert.ToDouble(S[1], provider));
               
            }
        }
        public double Sample(double time)
        {
            if (_time.Count == 0)
            {
                return 0;
            }
            if (_time[0] >= time)
            {
                return _value[0];
            }
            if (_time.LastOrDefault() <= time)
            {
                return _value.LastOrDefault();
            }
            var i = 0;
            while (_time[i] < time)
            {
                i++;
            }
            return (_value[i] - _value[i-1]) / (_time[i] - _time[i-1]) * (time - _time[i-1]) + _value[i-1];
        }
        
    }
}