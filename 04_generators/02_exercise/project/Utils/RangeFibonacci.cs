using System.Collections.Generic;

namespace Utils
{
    public class RangeFibonacci : Fibonacci
    {
        // TODO: Fields....
        private int _start;
        private int _stop;
        private int _step;
        // TODO: Constructor....
        public RangeFibonacci(int start = 0, int step = 1, int stop = 1)
        {
            _start = start;
            _step = step;
            _stop = stop;
        }
        public override IEnumerable<int> Numbers()
        {
            // TODO: Implement using version from base class....
            using IEnumerator<int> enumerator = base.Numbers().GetEnumerator();

            void NEnumeratorMove(int beg, int end)
            {
                while (beg <= end)
                {
                    enumerator.MoveNext();
                    beg++;
                }
            }
            NEnumeratorMove(0,_start);

            for (var i = _start + 1; i <= _stop; i += _step)
            {
                yield return enumerator.Current;
                NEnumeratorMove(0,_step - 1);
            }
            yield return enumerator.Current;
        }
    }
}