using System;
using System.Collections.Generic;

namespace Utils
{
    public class Fibonacci
    {
        public virtual IEnumerable<int> Numbers()
        {
            // TODO: ...
            int a = 0, b = 1;
            for (var i = 0; i < 44; i++)
            { 
                int res = a;
                a = b;
                b = res + b;
                yield return res;
            }
            throw new System.NotSupportedException();
        }
    }
}