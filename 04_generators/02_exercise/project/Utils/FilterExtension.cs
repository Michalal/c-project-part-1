using System.Collections.Generic;

namespace Utils
{
    public static class FilterExtension
    {
        // TODO: Even...
        public static IEnumerable<int> Even(this IEnumerable<int>? list)
        {
            if (list != null)
                foreach (var l in list)
                {
                    if (l % 2 == 0) 
                        yield return l;
                }
        }

        // TODO: Odd...
        public static IEnumerable<int> Odd(this IEnumerable<int>? list)
        {
            if (list != null)
                foreach (var l in list)
                {
                    if (l % 2 != 0) 
                        yield return l;
                }
        }

        // TODO: Only...
        public static IEnumerable<int> Only(this IEnumerable<int>? list, int count)
        {
            if (list != null)
                foreach (var l in list)
                {
                    yield return l;
                    if (l == count)  
                        break;
                }
        }
    }
}