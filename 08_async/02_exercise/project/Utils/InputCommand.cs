using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class InputCommand : Command
    {
        public InputCommand(string name = "input")
        {
            Name = name;
        }

        public override string Name { get; }

        public override async Task<string> RunAsync(CancellationToken cancellationToken)
        {
            return await Task.Run(Console.ReadLine, cancellationToken);
        }
        public override IEnumerable<Command> Continuations(string result, ICommandFactory commandFactory)
        {
            //throw new NotImplementedException();
            if (result == null) 
                throw new ArgumentNullException(nameof(result),"isNull"); 
            if (commandFactory == null) 
                throw new ArgumentNullException(nameof(commandFactory),"isNull");
            throw new NotImplementedException();
        }

    }
}