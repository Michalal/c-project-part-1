using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Utils
{
    public class Engine
    {
        private readonly ICommandFactory _commandFactory;
        private readonly List<Command> _commands;
        private readonly Dictionary<Task<string>, Command> _taskCommands;
        private readonly List<Task<string>> _tasks;

        public Engine(ICommandFactory factoryMockObject)
        {
            _commandFactory = factoryMockObject;
            _commands = new List<Command>();
            _taskCommands =  new Dictionary<Task<string>, Command>();
            _tasks = new List<Task<string>>();
        }

        public Task RunAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}