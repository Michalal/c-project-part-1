using System;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class SizeCommand : Command
    {
        private readonly IClient _client;

        public SizeCommand(Uri uri, IClient mockObject)
        {
            Uri = uri;
            _client = mockObject;
            Name = $"size '{uri}'";
        }

        public Uri Uri { get; }
        public override string Name { get; }

        public override async Task<string> RunAsync(CancellationToken cancellationToken)
        {
            var response = await _client.GetAsync(Uri, cancellationToken);
            return $"{Uri} {response.Length}";
        }
    }
}