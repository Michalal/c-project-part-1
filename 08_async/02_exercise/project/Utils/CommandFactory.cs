using System.Threading;

namespace Utils
{
    public class CommandFactory : ICommandFactory
    {
        public CommandFactory(CancellationTokenSource cancellationTokenSource)
        {
            CancellationTokenSource = cancellationTokenSource;
        }

        public CancellationTokenSource CancellationTokenSource { get; }

        public Command Create(params string[] arguments)
        {
            throw new System.NotImplementedException();
        }
    }
}