using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class DelayedPrintCommand : Command
    {
        public int Delay { get; }
        public string Message { get; }
        private readonly ITime _time;

        public DelayedPrintCommand(string message, int delay, ITime mockObject)
        {
            Message = message;
            Delay = delay;
            _time = mockObject;
            Name = $"delayed print '{message}' in {delay} ms";
        }

        public override string Name { get; }

        public override async Task<string> RunAsync(CancellationToken cancellationToken)
        {
            await _time.Delay(Delay, cancellationToken);
            return Message;
        }
    }
}