using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public abstract class Command
    {
        protected Command(string name="")
        {
            Name = name;
        }

        public virtual string Name { get; }
        public abstract Task<string> RunAsync(CancellationToken cancellationToken);

        public virtual IEnumerable<Command> Continuations(string result, ICommandFactory commandFactory)
        {
            return new List<Command>();
        }
    }
}