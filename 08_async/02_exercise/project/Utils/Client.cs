using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class Client : IClient
    {
        private readonly HttpClient _httpClient;

        public Client(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> GetAsync(Uri uri, CancellationToken token)
        {
            var response = await _httpClient.GetAsync(uri, token);
            return response.ToString();
        }
    }
}