using System.Threading;

namespace Utils
{
    public interface ICommandFactory
    {
        public CancellationTokenSource CancellationTokenSource { get; }
        public Command Create(params string[] arguments);
    }
}