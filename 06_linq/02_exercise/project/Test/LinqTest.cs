using System.Linq;
using Castle.Core.Internal;
using Utils;
using Xunit;

namespace Test
{
    public class LinqTest
    {
        private readonly Comment[] _comments;

        private readonly int[] _numbers;
        private readonly Post[] _posts;
        private readonly User[] _users;
        private readonly string[] _words;

        public LinqTest()
        {
            _numbers = new[] {7, 3, 4, 1, 3, 2, 3, 5, 4, 4, 5, 5, 6, 7};
            _words = new[] {"house", "plane", "snow", "dog", "cat", "pizza", "dotnet", "space"};

            _users = new[]
            {
                new User {Name = "John", Age = 12},
                new User {Name = "Tom", Age = 60},
                new User {Name = "Tim", Age = 22},
                new User {Name = "Stephan", Age = 100},
                new User {Name = "Peter"},
                new User {Age = 112},
                new User()
            };

            _posts = new[]
            {
                new Post(_users[0].Id) {Title = "invalid"},
                new Post(_users[0].Id) {Title = "Valid"},
                new Post(_users[1].Id) {Title = "invalid"},
                new Post(_users[2].Id) {Title = "invalid"},
                new Post(_users[3].Id) {Title = "Valid"},
                new Post(_users[3].Id) {Title = ""},
                new Post(_users[3].Id)
            };

            _comments = new[]
            {
                new Comment(_posts[0].Id) {Title = "Well done!", Text = "This it the best!"},
                new Comment(_posts[0].Id) {Title = "Mediocre...", Text = "You should try harder..."},
                new Comment(_posts[5].Id) {Title = "Hmm?", Text = "Missing title?"},
                new Comment(_posts[6].Id) {Title = "Why no title?", Text = "Isn't this invalid?"},
                new Comment(_posts[4].Id) {Title = "This is very long title.", Text = "This is short."}
            };
        }

        [Fact]
        public void SelectAdultUsers()
        {
            var result = _users.Where( user => user.Age > 18 );

            Assert.Equal(new[] {_users[1], _users[2], _users[3], _users[5]}, result);
        }

        [Fact]
        public void SelectAverageUserNameLengthForUsersWhoHaveName()
        {
            var result = _users.Where(user => user.Name != null).Average(user => user.Name!.Length);

            Assert.Equal(4.4, result, 2);
        }

        [Fact]
        public void SelectCommentsWhereTitleIsLongerThanText()
        {
            var result = _comments.Where(comment => comment.Title?.Length > comment.Text?.Length);

            Assert.Equal(new[] {_comments[4]}, result);
        }

        [Fact]
        public void SelectOnlyUserNameForAdultUsers()
        {
            var result = _users.Where(user => user.Age > 18).Select(user => user.Name);

            Assert.Equal(new[] {_users[1].Name, _users[2].Name, _users[3].Name, _users[5].Name}, result);
        }

        [Fact]
        public void SelectSumOfEvenNumbersAndSumOfOddNumbers()
        {
            var result =_numbers.GroupBy(number => number % 2 == 0).Select(number => (even: number.Key, sum: number.Sum()));;

            var resultArray = result.ToArray();
            Assert.Equal(2, resultArray.Length);
            Assert.Equal(resultArray[0], (even: false, sum: 39));
            Assert.Equal(resultArray[1], (even: true, sum: 20));
        }

        [Fact]
        public void SelectTotalNumberOfCharactersInAllWords()
        {
            var result = _words.Sum(word => word.Length);

            Assert.Equal(36, result);
        }

        [Fact]
        public void SelectUserCommentsForPostsWithNoTitle()
        {
            var result = _posts.Join(_users, post => post.UserId, user => user.Id, (post, user) => new {user, post}).Where(number => number.post.Title.IsNullOrEmpty()).Join(_comments, number => number.post.Id, comment => comment.PostId, (number, comment) => new {number.user, comment}).Select(number_2 => (number_2.user, number_2.comment));

            var resultArray = result.ToArray();
            Assert.Equal(2, resultArray.Length);
            Assert.Equal(resultArray[0], (_users[3], _comments[2]));
            Assert.Equal(resultArray[1], (_users[3], _comments[3]));
        }

        [Fact]
        public void SelectUsersWherePostTitleStartsWithUppercaseLetter()
        {
            var result = _users.Join(_posts, user => user.Id, post => post.UserId, (user, post) => new {user, post.Title}).Where(post => post.Title != null).Where(post => !post.Title.IsNullOrEmpty()).Where(post => char.IsUpper(post.Title![0])).Select(number => number.user);
          
            Assert.Equal(new[] {_users[0], _users[3]}, result);
        }

        [Fact]
        public void SelectUsersWithNameLongerThanThreeCharacters()
        {
            var result = _users.Where(user => user.Name?.Length > 3);

            Assert.Equal(new[] {_users[0], _users[3], _users[4]}, result);
        }

        [Fact]
        public void SelectUserWithNameTom()
        {
            var result = _users.Where(user => user.Name == "Tom");

            Assert.Equal(new[] {_users[1]}, result);
        }

        [Fact]
        public void SelectThreeLongestWordsInDescendingOrder()
        {
            var result = _words.OrderByDescending(word => word.Length).Take(3);

            Assert.Equal(new[] {"dotnet", "house", "plane"}, result);
        }
    }
}