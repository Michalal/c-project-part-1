using System.Diagnostics.CodeAnalysis;
using BenchmarkDotNet.Attributes;
using Utils;

namespace Bench
{
    [ExcludeFromCodeCoverage]
    [ShortRunJob]
    public class DemoBench
    {
        private readonly Demo _demo = GetDemo();

        private static Demo GetDemo()
        {
            var data = new int[10000000];

            for (var i = 0; i < data.Length; i++)
                data[i] = 1;

            return new Demo(data);
        }

        [Benchmark]
        public int Sum()
        {
            return _demo.Sum();
        }

        [Benchmark]
        public int SumForeach()
        {
            return _demo.SumForeach();
        }

        [Benchmark]
        public int SumLinq()
        {
            return _demo.SumLinq();
        }

        [Benchmark]
        [Arguments(2)]
        [Arguments(4)]
        [Arguments(8)]
        public int SumThreadsInterlocked(int threads)
        {
            return _demo.SumThreadsInterlocked(threads);
        }

        [Benchmark]
        [Arguments(2)]
        [Arguments(4)]
        [Arguments(8)]
        public int SumThreads(int threads)
        {
            return _demo.SumThreads(threads);
        }

        [Benchmark]
        [Arguments(2)]
        [Arguments(4)]
        [Arguments(8)]
        public int SumPoolThreads(int threads)
        {
            return _demo.SumPoolThreads(threads);
        }

        [Benchmark]
        public int SumTpl()
        {
            return _demo.SumTpl();
        }
    }
}