﻿using System.Diagnostics.CodeAnalysis;
using BenchmarkDotNet.Running;

namespace Bench
{
    [ExcludeFromCodeCoverage]
    internal static class Program
    {
        public static void Main(string[] args)
        {
            BenchmarkSwitcher.FromAssembly(typeof(Program).Assembly).Run(args);
        }
    }
}