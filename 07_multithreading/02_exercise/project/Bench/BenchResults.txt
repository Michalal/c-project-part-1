// AfterAll
// Benchmark Process 5333 has exited with code 0.

Mean = 2.717 ms, StdErr = 0.009 ms (0.32%), N = 3, StdDev = 0.015 ms
Min = 2.703 ms, Q1 = 2.709 ms, Median = 2.714 ms, Q3 = 2.723 ms, Max = 2.733 ms
IQR = 0.015 ms, LowerFence = 2.686 ms, UpperFence = 2.746 ms
ConfidenceInterval = [2.443 ms; 2.991 ms] (CI 99.9%), Margin = 0.274 ms (10.08% of Mean)
Skewness = 0.18, Kurtosis = 0.67, MValue = 2

// ***** BenchmarkRunner: Finish  *****

// * Export *
  BenchmarkDotNet.Artifacts/results/Bench.DemoBench-report.csv
  BenchmarkDotNet.Artifacts/results/Bench.DemoBench-report-github.md
  BenchmarkDotNet.Artifacts/results/Bench.DemoBench-report.html

// * Detailed results *
DemoBench.Sum: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3)
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 7.964 ms, StdErr = 0.040 ms (0.50%), N = 3, StdDev = 0.069 ms
Min = 7.892 ms, Q1 = 7.931 ms, Median = 7.971 ms, Q3 = 8.000 ms, Max = 8.029 ms
IQR = 0.069 ms, LowerFence = 7.828 ms, UpperFence = 8.103 ms
ConfidenceInterval = [6.708 ms; 9.219 ms] (CI 99.9%), Margin = 1.255 ms (15.76% of Mean)
Skewness = -0.1, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[7.829 ms ; 8.092 ms) | @@@
---------------------------------------------------

DemoBench.SumForeach: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3)
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 6.962 ms, StdErr = 0.082 ms (1.18%), N = 3, StdDev = 0.142 ms
Min = 6.800 ms, Q1 = 6.910 ms, Median = 7.020 ms, Q3 = 7.043 ms, Max = 7.065 ms
IQR = 0.132 ms, LowerFence = 6.712 ms, UpperFence = 7.241 ms
ConfidenceInterval = [4.376 ms; 9.548 ms] (CI 99.9%), Margin = 2.586 ms (37.15% of Mean)
Skewness = -0.34, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[6.671 ms ; 6.914 ms) | @
[6.914 ms ; 7.194 ms) | @@
---------------------------------------------------

DemoBench.SumLinq: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3)
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 48.204 ms, StdErr = 0.203 ms (0.42%), N = 3, StdDev = 0.351 ms
Min = 47.801 ms, Q1 = 48.085 ms, Median = 48.369 ms, Q3 = 48.405 ms, Max = 48.442 ms
IQR = 0.320 ms, LowerFence = 47.604 ms, UpperFence = 48.886 ms
ConfidenceInterval = [41.802 ms; 54.605 ms] (CI 99.9%), Margin = 6.402 ms (13.28% of Mean)
Skewness = -0.37, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[47.481 ms ; 48.761 ms) | @@@
---------------------------------------------------

DemoBench.SumTpl: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3)
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 11.900 ms, StdErr = 0.317 ms (2.66%), N = 3, StdDev = 0.549 ms
Min = 11.319 ms, Q1 = 11.645 ms, Median = 11.971 ms, Q3 = 12.190 ms, Max = 12.409 ms
IQR = 0.545 ms, LowerFence = 10.827 ms, UpperFence = 13.008 ms
ConfidenceInterval = [1.889 ms; 21.911 ms] (CI 99.9%), Margin = 10.011 ms (84.13% of Mean)
Skewness = -0.13, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[10.820 ms ; 11.691 ms) | @
[11.691 ms ; 12.690 ms) | @@
---------------------------------------------------

DemoBench.SumThreadsInterlocked: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=2]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 71.953 ms, StdErr = 1.401 ms (1.95%), N = 3, StdDev = 2.426 ms
Min = 70.501 ms, Q1 = 70.553 ms, Median = 70.604 ms, Q3 = 72.679 ms, Max = 74.754 ms
IQR = 2.126 ms, LowerFence = 67.364 ms, UpperFence = 75.868 ms
ConfidenceInterval = [27.697 ms; 116.209 ms] (CI 99.9%), Margin = 44.256 ms (61.51% of Mean)
Skewness = 0.38, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[70.420 ms ; 74.835 ms) | @@@
---------------------------------------------------

DemoBench.SumThreads: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=2]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 16.558 ms, StdErr = 1.164 ms (7.03%), N = 3, StdDev = 2.016 ms
Min = 14.499 ms, Q1 = 15.573 ms, Median = 16.648 ms, Q3 = 17.588 ms, Max = 18.528 ms
IQR = 2.015 ms, LowerFence = 12.551 ms, UpperFence = 20.611 ms
ConfidenceInterval = [-20.229 ms; 53.346 ms] (CI 99.9%), Margin = 36.787 ms (222.17% of Mean)
Skewness = -0.04, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[12.664 ms ; 15.753 ms) | @
[15.753 ms ; 19.423 ms) | @@
---------------------------------------------------

DemoBench.SumPoolThreads: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=2]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 4.261 ms, StdErr = 0.115 ms (2.70%), N = 3, StdDev = 0.199 ms
Min = 4.141 ms, Q1 = 4.146 ms, Median = 4.152 ms, Q3 = 4.321 ms, Max = 4.490 ms
IQR = 0.175 ms, LowerFence = 3.884 ms, UpperFence = 4.584 ms
ConfidenceInterval = [0.632 ms; 7.890 ms] (CI 99.9%), Margin = 3.629 ms (85.16% of Mean)
Skewness = 0.38, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[4.134 ms ; 4.496 ms) | @@@
---------------------------------------------------

DemoBench.SumThreadsInterlocked: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=4]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 99.170 ms, StdErr = 4.713 ms (4.75%), N = 3, StdDev = 8.164 ms
Min = 90.240 ms, Q1 = 95.630 ms, Median = 101.020 ms, Q3 = 103.635 ms, Max = 106.249 ms
IQR = 8.005 ms, LowerFence = 83.623 ms, UpperFence = 115.642 ms
ConfidenceInterval = [-49.765 ms; 248.105 ms] (CI 99.9%), Margin = 148.935 ms (150.18% of Mean)
Skewness = -0.22, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[82.810 ms ;  96.206 ms) | @
[96.206 ms ; 113.678 ms) | @@
---------------------------------------------------

DemoBench.SumThreads: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=4]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 12.789 ms, StdErr = 2.975 ms (23.26%), N = 3, StdDev = 5.153 ms
Min = 9.798 ms, Q1 = 9.814 ms, Median = 9.830 ms, Q3 = 14.285 ms, Max = 18.739 ms
IQR = 4.471 ms, LowerFence = 3.108 ms, UpperFence = 20.990 ms
ConfidenceInterval = [-81.215 ms; 106.793 ms] (CI 99.9%), Margin = 94.004 ms (735.04% of Mean)
Skewness = 0.38, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[9.579 ms ; 18.957 ms) | @@@
---------------------------------------------------

DemoBench.SumPoolThreads: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=4]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 3.048 ms, StdErr = 0.198 ms (6.51%), N = 3, StdDev = 0.344 ms
Min = 2.806 ms, Q1 = 2.852 ms, Median = 2.898 ms, Q3 = 3.169 ms, Max = 3.441 ms
IQR = 0.318 ms, LowerFence = 2.375 ms, UpperFence = 3.646 ms
ConfidenceInterval = [-3.219 ms; 9.316 ms] (CI 99.9%), Margin = 6.268 ms (205.62% of Mean)
Skewness = 0.35, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[2.493 ms ; 3.164 ms) | @@
[3.164 ms ; 3.754 ms) | @
---------------------------------------------------

DemoBench.SumThreadsInterlocked: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=8]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 109.118 ms, StdErr = 13.428 ms (12.31%), N = 3, StdDev = 23.258 ms
Min = 87.997 ms, Q1 = 96.655 ms, Median = 105.312 ms, Q3 = 119.678 ms, Max = 134.044 ms
IQR = 23.023 ms, LowerFence = 62.120 ms, UpperFence = 154.213 ms
ConfidenceInterval = [-315.195 ms; 533.430 ms] (CI 99.9%), Margin = 424.312 ms (388.86% of Mean)
Skewness = 0.16, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[ 75.489 ms ; 117.820 ms) | @@
[117.820 ms ; 155.209 ms) | @
---------------------------------------------------

DemoBench.SumThreads: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=8]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 21.142 ms, StdErr = 2.179 ms (10.31%), N = 3, StdDev = 3.775 ms
Min = 16.952 ms, Q1 = 19.573 ms, Median = 22.195 ms, Q3 = 23.237 ms, Max = 24.279 ms
IQR = 3.663 ms, LowerFence = 14.079 ms, UpperFence = 28.731 ms
ConfidenceInterval = [-47.726 ms; 90.010 ms] (CI 99.9%), Margin = 68.868 ms (325.74% of Mean)
Skewness = -0.26, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[13.517 ms ; 19.801 ms) | @
[19.801 ms ; 27.714 ms) | @@
---------------------------------------------------

DemoBench.SumPoolThreads: ShortRun(IterationCount=3, LaunchCount=1, WarmupCount=3) [threads=8]
Runtime = .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT; GC = Concurrent Workstation
Mean = 2.717 ms, StdErr = 0.009 ms (0.32%), N = 3, StdDev = 0.015 ms
Min = 2.703 ms, Q1 = 2.709 ms, Median = 2.714 ms, Q3 = 2.723 ms, Max = 2.733 ms
IQR = 0.015 ms, LowerFence = 2.686 ms, UpperFence = 2.746 ms
ConfidenceInterval = [2.443 ms; 2.991 ms] (CI 99.9%), Margin = 0.274 ms (10.08% of Mean)
Skewness = 0.18, Kurtosis = 0.67, MValue = 2
-------------------- Histogram --------------------
[2.690 ms ; 2.736 ms) | @@@
---------------------------------------------------

// * Summary *

BenchmarkDotNet=v0.13.1, OS=ubuntu 21.04
Intel Core i5-10210U CPU 1.60GHz, 1 CPU, 4 logical and 4 physical cores
.NET SDK=5.0.401
  [Host]   : .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT
  ShortRun : .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT

Job=ShortRun  IterationCount=3  LaunchCount=1  
WarmupCount=3  

|                Method | threads |       Mean |       Error |     StdDev |     Median |
|---------------------- |-------- |-----------:|------------:|-----------:|-----------:|
|                   Sum |       ? |   7.964 ms |   1.2554 ms |  0.0688 ms |   7.971 ms |
|            SumForeach |       ? |   6.962 ms |   2.5861 ms |  0.1418 ms |   7.020 ms |
|               SumLinq |       ? |  48.204 ms |   6.4015 ms |  0.3509 ms |  48.369 ms |
|                SumTpl |       ? |  11.900 ms |  10.0109 ms |  0.5487 ms |  11.971 ms |
| SumThreadsInterlocked |       2 |  71.953 ms |  44.2561 ms |  2.4258 ms |  70.604 ms |
|            SumThreads |       2 |  16.558 ms |  36.7872 ms |  2.0164 ms |  16.648 ms |
|        SumPoolThreads |       2 |   4.261 ms |   3.6287 ms |  0.1989 ms |   4.152 ms |
| SumThreadsInterlocked |       4 |  99.170 ms | 148.9349 ms |  8.1636 ms | 101.020 ms |
|            SumThreads |       4 |  12.789 ms |  94.0038 ms |  5.1527 ms |   9.830 ms |
|        SumPoolThreads |       4 |   3.048 ms |   6.2677 ms |  0.3436 ms |   2.898 ms |
| SumThreadsInterlocked |       8 | 109.118 ms | 424.3124 ms | 23.2580 ms | 105.312 ms |
|            SumThreads |       8 |  21.142 ms |  68.8678 ms |  3.7749 ms |  22.195 ms |
|        SumPoolThreads |       8 |   2.717 ms |   0.2739 ms |  0.0150 ms |   2.714 ms |

// * Legends *
  threads : Value of the 'threads' parameter
  Mean    : Arithmetic mean of all measurements
  Error   : Half of 99.9% confidence interval
  StdDev  : Standard deviation of all measurements
  Median  : Value separating the higher half of all measurements (50th percentile)
  1 ms    : 1 Millisecond (0.001 sec)

// ***** BenchmarkRunner: End *****
// ** Remained 0 benchmark(s) to run **
Run time: 00:01:17 (77.14 sec), executed benchmarks: 13

Global total time: 00:01:22 (82.35 sec), executed benchmarks: 13
// * Artifacts cleanup *

Process finished with exit code 0.
