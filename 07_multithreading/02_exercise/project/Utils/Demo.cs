﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class Demo
    {
        private readonly int[] _data;

        public Demo(int[] data)
        {
            _data = data;
        }

        public int Sum()
        {
            // TODO: Use for loop to calculate sum
            var result = 0;
            for (var i = 0; i < _data.Length; i++)
            {
                result += _data[i];
            }
            return result;
        }

        public int SumForeach()
        {
            // TODO: Use foreach loop to calculate sum
            var result = 0;
            foreach (var i in _data)
            {
                result += i;
            }
            return result;
        }

        public int SumLinq()
        {
            // TODO: Use for LINQ to calculate sum
            return _data.Sum();
        }

        private void RunStandaloneThreads(int count, ThreadAction action)
        {
            // TODO: Run 'count' threads and execute 'action' in every one with appropriate range of data
            var threadList = new List<Thread>();
            for(var i = 0; i < count; i++)
            {
                var start = _data.Length / count * i;
                var stop = _data.Length / count * (i + 1);
                if(i == count - 1)
                    stop += _data.Length % count;
                threadList.Add(new Thread(() => action(start, stop)));
            }
            for(var i = 0; i < count; i++)
            {
                threadList[i].Start();
                threadList[i].Join();
            }
        }

        public int SumThreadsInterlocked(int count)
        {
            // TODO: Use 'Interlocked.Add' to calculate total sum
            // TODO: Use 'RunStandaloneThreads' to run threads
            // TODO: Ue lambda to poss 'action' to 'RunStandaloneThreads'
            var result = 0;
            RunStandaloneThreads(count, (start, stop) =>
            {
                for (var i = start; i < stop; i++)
                {
                    Interlocked.Add(ref result, _data[i]);
                }    
            });
            return result;
        }


        public int SumThreads(int count)
        {
            // TODO: Use 'Interlocked.Add' to calculate total sum
            // TODO: Use partial sum in threads to avoid excessive use of 'Interlocked.Add'
            // TODO: Use 'RunStandaloneThreads' to run threads
            // TODO: Ue lambda to poss 'action' to 'RunStandaloneThreads'
            var result = 0;
            RunStandaloneThreads(count, (start, stop) => 
            {
                var sum = 0;
                for (var i = start; i < stop; i++) 
                {
                    sum += _data[i];
                }
                Interlocked.Add(ref result, sum);
            });
            return result;
        }

        private void RunPoolThreads(int count, ThreadAction action)
        {
            // TODO: Run 'count' pool threads and execute 'action' in every one with appropriate range of data
            // TODO: Use 'ManualResetEvent' to synchronize operations
            var events = new ManualResetEvent[count];
            var start = 0.0;
            for (var i = 0; i < count; i++)
            {
                events[i] = new ManualResetEvent(false);
                var copyIndex = i;
                var copyStart = (int)(start);
                var copyStop = (int)(start + (double) _data.Length / count);
                ThreadPool.QueueUserWorkItem(_ =>
                { 
                    action(copyStart, copyStop);
                    events[copyIndex].Set();
                });
                start += (double) _data.Length / count;
            }
            WaitHandle.WaitAll(events.ToArray<WaitHandle>());
        }

        public int SumPoolThreads(int count)
        {
            // TODO: Use 'RunPoolThreads' to run threads
            // TODO: Use 'Interlocked.Add' to aggregate data
            // TODO: Ue lambda to poss 'action' to 'RunPoolThreads'
            var result = 0;
            RunPoolThreads(count, (start, stop) => 
            {
                var sum = 0;
                for (var i = start; i < stop; i++) 
                {
                    sum += _data[i];
                }
                Interlocked.Add(ref result, sum);
            });
            return result;
        }

        public int SumTpl()
        {
            // TODO: Use 'Parallel.For' to calculate sum
            // TODO: Ue lambdas to poss operations
            var result = 0;
            Parallel.For(0, _data.Length, () => 0, (i, _, sum) =>
            {
                sum += _data[i];
                return sum;
            }, sum => Interlocked.Add(ref result, sum));
            return result;
        }

        private delegate void ThreadAction(int start, int stop);
    }
}