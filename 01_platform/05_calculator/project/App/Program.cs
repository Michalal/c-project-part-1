﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Utils;
using static System.Convert;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main(string[] args)
        {
            if(args.Length<2)
                return;
            if (args[1]=="add")
            {
                var c = new Calculator(Convert.ToInt32(args[0], CultureInfo.CurrentCulture), Convert.ToInt32(args[2], CultureInfo.CurrentCulture));
                Console.WriteLine(c.Add());
            }
            else if (args[1]=="sub")
            {
                var c = new Calculator(Convert.ToInt32(args[0], CultureInfo.CurrentCulture), Convert.ToInt32(args[2], CultureInfo.CurrentCulture));
                Console.WriteLine(c.Sub());
            }
            else if (args[1]=="mul")
            {
                var c = new Calculator(Convert.ToInt32(args[0], CultureInfo.CurrentCulture), Convert.ToInt32(args[2], CultureInfo.CurrentCulture));
                Console.WriteLine(c.Mul());
            }
            else if (args[1]=="div")
            {
                var c = new Calculator(Convert.ToInt32(args[0], CultureInfo.CurrentCulture), Convert.ToInt32(args[2], CultureInfo.CurrentCulture));
                Console.WriteLine(c.Div());
            }
        }
    }
}