using NUnit.Framework;
using Utils;

namespace Test
{
    public class CalculatorTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var testAdd = new Calculator(21,37);
            var res = testAdd.Add();
            Assert.AreEqual(res, 58);
        }
        
        [Test]
        public void Test2()
        {
            var testAdd = new Calculator(21,37);
            var res = testAdd.Sub();
            Assert.AreEqual(res, -16);
        }
        
        [Test]
        public void Test3()
        {
            var testAdd = new Calculator(21,37);
            var res = testAdd.Mul();
            Assert.AreEqual(res, 777);
        }
        
        [Test]
        public void Test4()
        {
            var testAdd = new Calculator(6,2);
            var res = testAdd.Div();
            Assert.AreEqual(res, 3);
        }
    }
}