using Xunit;
using Utils;

namespace Test
{
    public class TestClickedEvent
    {
        [Fact]
        public void CheckIfSettingLabelAndGettingItWorkProperly()
        {
            var res = new ClickedEventArgs("OkejOkejOkej");

            var val = res.Label;
            
            Assert.Equal("OkejOkejOkej",val);
        }
    }
}