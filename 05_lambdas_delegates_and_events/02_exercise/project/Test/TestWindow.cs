using Utils;
using Xunit;

namespace Test
{
    public class TestWindow
    {
        [Fact]
        public void CreateNewWindowAndCheckValuesForHandledButton()
        {
            var w = new Window();
            w.SimulateClicks();
            var valok = w.HandledButtonOkClick;
            var valcancel = w.HandledButtonCancelClick;
            Assert.Equal(valok,valcancel);
            Assert.True(valcancel);
            Assert.True(valok);
        }
    }
}