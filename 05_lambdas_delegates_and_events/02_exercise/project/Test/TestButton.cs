using Utils;
using Xunit;

namespace Test
{
    public class TestButton
    {
        [Fact]
        public void CheckActionsDuringCreationButtonAndAfterIt()
        {
            var b = new Button("OkejOkejOkej");
            var val = b.Label;
            Assert.Equal("OkejOkejOkej", val);
        }
    }
}