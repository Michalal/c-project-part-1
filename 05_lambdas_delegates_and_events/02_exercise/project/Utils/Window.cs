using System;

namespace Utils
{
    public partial class Window
    {
        private void ButtonOk_Clicked(object sender, ClickedEventArgs e) 
        {
            HandledButtonOkClick = true;
            Console.WriteLine(e.Label);
        }
        private void ButtonCancel_Clicked(object sender, ClickedEventArgs e) 
        {
            HandledButtonCancelClick = true;
            Console.WriteLine(e.Label);
        }

        public bool HandledButtonOkClick
        {
            get; 
            private set;
        }

        public bool HandledButtonCancelClick
        {
            get; 
            private set;
        }
        
        public Window() 
        {
            InitializeComponent();
        }

        public void SimulateClicks()
        {
            _buttonOk?.Click();
            _buttonCancel?.Click();        
        }
    }
}