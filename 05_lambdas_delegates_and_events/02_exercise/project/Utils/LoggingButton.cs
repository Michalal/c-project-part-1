using System;

namespace Utils
{
    public class LoggingButton : Button
    {
        protected override void OnClicked() 
        {
            Console.WriteLine("Click "+Label);
            base.OnClicked();
        }

        public LoggingButton(string label) : base(label)
        {
            
        }
    }
}