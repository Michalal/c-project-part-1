namespace Utils
{
    public partial class Window
    {
        private LoggingButton _buttonOk;
        private LoggingButton _buttonCancel;
        
        private void InitializeComponent() 
        {
            _buttonOk = new LoggingButton("Ok");
            _buttonOk.Clicked += ButtonOk_Clicked;
            _buttonCancel = new LoggingButton("Cancel");
            _buttonCancel.Clicked += ButtonCancel_Clicked;
        }
    }
}