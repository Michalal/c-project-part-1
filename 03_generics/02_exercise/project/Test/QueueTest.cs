using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class QueueTest
    {
        [Fact]
        public void QueueOfIntBasicOperations()
        {
            // TODO: ...
            Queue<int> queue = new();

            Assert.Empty(queue);

            // TODO: ...
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            Assert.Equal(3, queue.Count);

            int NNext()
            {
                return queue.Dequeue();
            }

            Assert.Equal(3, queue.Count);
            Assert.Equal(1, NNext());
            Assert.Equal(2, NNext());
            Assert.Equal(3, NNext());
            Assert.Empty(queue);
        }

        [Fact]
        public void QueueOfIntCheckNextValueWithoutRemovingIt()
        {
            // TODO: ...
            Queue<int> queue = new();
            Assert.Empty(queue);

            // TODO: ...
            queue.Enqueue(1);
            queue.Enqueue(2);
            Assert.Equal(2, queue.Count);

            // TODO: ...
            var value = queue.Peek();
            Assert.Equal(1, value);

            int NNext()
            {
                // TODO: ...
                return queue.Dequeue();
            }

            Assert.Equal(1, NNext());
            Assert.Equal(2, NNext());

            Assert.Empty(queue);
        }
    }
}