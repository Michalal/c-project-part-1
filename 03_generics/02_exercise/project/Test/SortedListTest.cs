using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class SortedListTest
    {
        // TODO: ReversedComparer class...
        private class ReversedComparer : IComparer<int>
        {
            public int Compare(int x,int y) 
            {
                return -1 * x.CompareTo(y);
            }
        }

        [Fact]
        public void SortedListOfIntToStringIsSortedByKey()
        {
            // TODO: ...
            SortedList<int, string> list = new();
            list.Add(0,"C");
            list.Add(1,"A");
            list.Add(2,"B");

            Assert.Equal(3, list.Count);

            Assert.Equal("C", list[0]);
            Assert.Equal("A", list[1]);
            Assert.Equal("B", list[2]);

            Assert.Equal(0, list.Keys[0]);
            Assert.Equal(1, list.Keys[1]);
            Assert.Equal(2, list.Keys[2]);

            Assert.Equal("C", list.Values[0]);
            Assert.Equal("A", list.Values[1]);
            Assert.Equal("B", list.Values[2]);
        }

        [Fact]
        public void SortedListOfIntToStringWithCustomComparer()
        {
            // TODO: ...
            SortedList<int, String> list = new SortedList<int, String>(new ReversedComparer());
            list.Add(0, "C");
            list.Add(1, "A");
            list.Add(2,"B");
            Assert.IsType<ReversedComparer>(list.Comparer);

            Assert.Equal(3, list.Count);

            Assert.Equal("C", list[0]);
            Assert.Equal("A", list[1]);
            Assert.Equal("B", list[2]);

            Assert.Equal(2, list.Keys[0]);
            Assert.Equal(1, list.Keys[1]);
            Assert.Equal(0, list.Keys[2]);

            Assert.Equal("B", list.Values[0]);
            Assert.Equal("A", list.Values[1]);
            Assert.Equal("C", list.Values[2]);
        }
    }
}