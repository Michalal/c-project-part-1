using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class DictionaryTest
    {
        [Fact]
        public void DictionaryOfInToIntChangeExistingValue()
        {
            // TODO: ...
            Dictionary<int,int> dictionary = new();
            dictionary.Add(1,10);
            Assert.Single(dictionary);
            Assert.Equal(10, dictionary[1]);

            // TODO: ...
            dictionary.Remove(1);
            dictionary.Add(1,20);

            Assert.Single(dictionary);
            Assert.Equal(20, dictionary[1]);
        }

        [Fact]
        public void DictionaryOfInToIntRemoveValues()
        {
            // TODO: ...
            Dictionary<int,int> dictionary = new();
            dictionary.Add(1,21);
            dictionary.Add(2,37);
            dictionary.Add(3,88);
            Assert.Equal(3, dictionary.Count);
            Assert.True(dictionary.ContainsKey(1));
            Assert.True(dictionary.ContainsKey(2));
            Assert.True(dictionary.ContainsKey(3));

            // TODO: ...
            dictionary.Remove(2);
            dictionary.Remove(3);
            Assert.Single(dictionary);
            Assert.True(dictionary.ContainsKey(1));
            Assert.False(dictionary.ContainsKey(2));
            Assert.False(dictionary.ContainsKey(3));
        }

        [Fact]
        public void DictionaryOfInToIntTryGetValue()
        {
            // TODO: ...
            Dictionary<int,int> dictionary = new();
            dictionary.Add(1,10);

            Assert.Single(dictionary);
            Assert.Equal(10, dictionary[1]);

            // TODO: ...

            int value;
            bool hasValue = dictionary.TryGetValue(1, out value);
            Assert.True(hasValue);
            Assert.Equal(10, value);

            Assert.Single(dictionary);
        }

        [Fact]
        public void DictionaryOfIntToIntCanBeCreatedInOneLine()
        {
            // TODO: ...
            Dictionary<int,int> dictionary = new(){{1,2},{3,4}};
            Assert.Equal(2, dictionary.Count);
            Assert.Equal(2, dictionary[1]);
            Assert.Equal(4, dictionary[3]);
        }

        [Fact]
        public void DictionaryOfStringToStringThrowsWheKeyNotInDictionary()
        {
            // TODO: ...
            Dictionary<string,string> dictionary = new();
            dictionary.Add("A","B");
            Assert.Single(dictionary);
            Assert.Equal("B", dictionary["A"]);

            void NAccessElement()
            {
                // TODO: ...
                try
                {
                    Console.WriteLine(dictionary["C"]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            Assert.Throws<KeyNotFoundException>(NAccessElement);

            // TODO: ...
            dictionary.Add("C","D");

            Assert.Equal(2, dictionary.Count);

            NAccessElement();
        }

        [Fact]
        public void DictionaryOfStringToStringThrowsWhenAddedElementExists()
        {
            // TODO: ...
            Dictionary<string,string> dictionary = new();
            dictionary.Add("A","B");
            Assert.Single(dictionary);
            Assert.Equal("B", dictionary["A"]);

            void NAddElement()
            {
                // TODO: ...
                try
                {
                    dictionary.Add("A", "C");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            Assert.Throws<ArgumentException>(NAddElement);

            // TODO: ...
            dictionary.Remove("A");
            Assert.Empty(dictionary);

            NAddElement();

            Assert.Single(dictionary);
            Assert.Equal("C", dictionary["A"]);
        }
    }
}