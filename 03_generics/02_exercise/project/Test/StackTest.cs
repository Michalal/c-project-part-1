using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class StackTest
    {
        [Fact]
        public void StackOfIntBasicOperations()
        {
            // TODO: ...
            Stack<int> stack = new();
            Assert.Empty(stack);

            // TODO: ...
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            Assert.Equal(3, stack.Count);

            int NNext()
            {
                // TODO: ...
                return stack.Pop();
            }

            Assert.Equal(3, stack.Count);

            Assert.Equal(3, NNext());
            Assert.Equal(2, stack.Count);

            Assert.Equal(2, NNext());
            Assert.Single(stack);

            Assert.Equal(1, NNext());
            Assert.Empty(stack);
        }
    }
}