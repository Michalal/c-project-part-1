using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class HashSetTest
    {
        [Fact]
        public void HashSetStringBasicOperations()
        {
            // TODO: ...
            HashSet<string> hashSet = new();
            hashSet.Add("A");
            hashSet.Add("B");
            hashSet.Add("C");

            Assert.Equal(3, hashSet.Count);
            Assert.Contains("A", hashSet);
            Assert.Contains("B", hashSet);
            Assert.Contains("C", hashSet);

            // TODO: ...
            hashSet.Remove("A");

            Assert.Equal(2, hashSet.Count);
            Assert.DoesNotContain("A", hashSet);
            Assert.Contains("B", hashSet);
            Assert.Contains("C", hashSet);
        }

        [Fact]
        public void HashSetStringIsCommonPart()
        {
            // TODO: ...
            HashSet<string> hashSetA = new();
            hashSetA.Add("A");
            hashSetA.Add("B");
            hashSetA.Add("C");

            Assert.Equal(3, hashSetA.Count);
            Assert.Contains("A", hashSetA);
            Assert.Contains("B", hashSetA);
            Assert.Contains("C", hashSetA);

            // TODO: ...
            HashSet<string> hashSetB = new();
            hashSetB.Add("A");
            hashSetB.Add("B");
            hashSetB.Add("F");
            
            Assert.Equal(3, hashSetB.Count);
            Assert.Contains("A", hashSetB);
            Assert.Contains("B", hashSetB);
            Assert.Contains("F", hashSetB);

            // TODO: ...
            hashSetA.Remove("C");

            Assert.Equal(2, hashSetA.Count);
            Assert.Contains("A", hashSetA);
            Assert.Contains("B", hashSetA);

            Assert.Equal(3, hashSetB.Count);
            Assert.Contains("A", hashSetB);
            Assert.Contains("B", hashSetB);
            Assert.Contains("F", hashSetB);
        }

        [Fact]
        public void HashSetStringIsSubsetOfOtherSet()
        {
            // TODO: ...
            HashSet<string> hashSetSuperset = new();
            HashSet<string> hashSetSubset = new();
            hashSetSuperset.Add("A");
            hashSetSuperset.Add("B");
            hashSetSuperset.Add("C");
            hashSetSuperset.Add("D");
            hashSetSuperset.Add("E");
            hashSetSubset.Add("C");
            hashSetSubset.Add("D");
            hashSetSubset.Add("E");

            Assert.Equal(5, hashSetSuperset.Count);
            Assert.Equal(3, hashSetSubset.Count);

            Assert.True(hashSetSuperset.Overlaps(hashSetSubset));
            Assert.True(hashSetSubset.Overlaps(hashSetSuperset));
        }
    }
}