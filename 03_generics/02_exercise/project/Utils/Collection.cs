﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    // TODO: Generic Collection<T> class...
    // TODO: Elements stored in plain array (T[] _elements)...
    public class Collection<T> : IEnumerable<T>
    {
        private T[] _collection;
        private readonly int _factor;
        private int _count;
        private int _capacity;
        public Collection(int i = 1, int i1 = 2)
        {
            _factor = i1;
            _count = 0;
            _capacity = i;
            _collection = new T[_capacity];
        }

        public int Factor
        {
            get => _factor;
        }
        public int Count
        {
            get => _count;
            set => _count = value;
        }
        public int Capacity
        {
            get => _capacity;
            set => _capacity = value;
        }

        public T this[int i] => _collection[i];
        public IEnumerator <T> GetEnumerator()
        {
            return _collection.Take(Count).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void Add(T p0)
        {
            if (Count >= Capacity) 
            {
                Array.Resize(ref _collection, Factor * Capacity);
                Capacity = Factor * Capacity;
            }
            _collection[Count++] = p0;   
        }
    }
    
}